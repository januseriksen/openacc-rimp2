program rimp2_gpu_2

  use iso_c_binding
  use omp_lib
  use openacc
  use tools
  use cuda_interface
  use cublas_interface

  implicit none

  ! number of occupied orbitals (nocc), virtual orbitals (nvirt), and auxilliary basis functions (naux)
  integer :: nocc, nvirt, naux
  ! fitting coefficients
  real(8), allocatable, dimension(:,:,:) :: Calpha
  ! occupied (EpsOcc) and virtual (EpsVirt) orbital energies
  real(8), allocatable, dimension(:) :: EpsOcc, EpsVirt
  ! RI-MP2 energy
  real(8) :: rimp2_energy
  ! orbital energy differences
  real(8) :: eps_ijb,eps
  ! nvirt,nvirt tile of MO integrals
  real(8), allocatable, dimension(:,:) :: g_ij
  ! permutational symmetry factor
  real(8) :: pre
  ! loop indices
  integer :: j, i, b, a
  ! timings
  real(8) :: w_time_start, w_time_tot 
  ! device memory bookkeeping
  integer(c_size_t) :: total_gpu_i, total_gpu_f, free_gpu_i, free_gpu_f ! in bytes
  real(8), parameter :: gb = 1073741824.0d0 ! 1 GB
  ! allocation / deallocation status
  integer :: alloc_stat, dealloc_stat
  ! input strings
  character(10) :: nocc_string, nvirt_string, naux_string, acc_num_dev_string
  ! CUBLAS stat
  integer(4) :: stat
  ! CUBLAS handle
  type(c_ptr) :: cublas_handle
  ! device type
  integer(kind=acc_device_kind) :: acc_device_type
  ! number of devices
  integer :: acc_num_devices
  ! maximum number of devices
  integer :: acc_max_num_devices

  ! read in dimensions
  call get_command_argument ( 1 , nocc_string )
  read (nocc_string,*) nocc
  call get_command_argument ( 2 , nvirt_string )
  read (nvirt_string,*) nvirt
  call get_command_argument ( 3 , naux_string )
  read (naux_string,*) naux
  call get_command_argument ( 4 , acc_num_dev_string )
  read (acc_num_dev_string,*) acc_num_devices

  ! query for device type
  acc_device_type = acc_get_device_type()
  call acc_init ( acc_device_type )

  ! query for number of devices
  acc_max_num_devices = 1
  if (acc_num_devices .gt. acc_max_num_devices) stop "*** acc_num_devices .gt. 1 ***"
  if (acc_num_devices .lt. 1) stop "*** acc_num_devices must be .eq. 1 ***"

  print *,''
  write (*,'("GPU-2: nocc = ",I4,"  ---  nvirt = ",I4,"  ---  naux = ",I4,&
             & "  ---  devices = ",I2," /",I2)') nocc,nvirt,naux,acc_num_devices,acc_max_num_devices

  ! allocate tensors
  allocate ( Calpha(naux,nvirt,nocc) , stat = alloc_stat )
  if (alloc_stat .ne. 0) stop "*** insufficient memory (Calpha) ***"
  allocate ( EpsOcc(nocc) , stat = alloc_stat )
  if (alloc_stat .ne. 0) stop "*** insufficient memory (EpsOcc) ***"
  allocate ( EpsVirt(nvirt) , stat = alloc_stat )
  if (alloc_stat .ne. 0) stop "*** insufficient memory (EpsVirt) ***"

  ! fill the tensors with "random" numbers
  call fill_tensors_dp ( nocc,nvirt,naux,Calpha,EpsOcc,EpsVirt )

  ! allocate g_ij tensor
  allocate ( g_ij(nvirt,nvirt) , stat = alloc_stat )
  if (alloc_stat .ne. 0) stop "*** insufficient memory (g_ij) ***"

  ! initialize the cublas context
  stat = cublasCreate_v2 ( cublas_handle )

  rimp2_energy = 0.0d0

  call get_dev_mem ( total_gpu_i,free_gpu_i )

  w_time_start = omp_get_wtime()

!$acc data copyin(Calpha,EpsOcc,EpsVirt) copy(rimp2_energy) create(g_ij)

  do j = 1,nocc
     do i = j,nocc

        call dgemm_interface ( 't','n',nvirt,nvirt,naux,1.0d0,Calpha(:,:,j),naux,Calpha(:,:,i),naux,0.0d0,g_ij,nvirt,&
                           & naux*nvirt,naux*nvirt,nvirt**2,cublas_handle,.true. )

!$acc parallel loop gang independent present(g_ij,EpsOcc,EpsVirt,rimp2_energy) &
!$acc& firstprivate(nvirt,j,i) private(b,a,pre,eps_ijb,eps) &
!$acc& reduction(+:rimp2_energy)
        do b = 1,nvirt
!$acc cache(EpsOcc(i),EpsOcc(j))

           eps_ijb = EpsOcc(i) + EpsOcc(j) - EpsVirt(b)

!$acc loop vector independent reduction(+:rimp2_energy)
           do a = b,nvirt

              if ((i .eq. j) .and. (a .eq. b)) then
                 pre = 1.0d0
              elseif ((i .eq. j) .and. (a .ne. b)) then
                 pre = 2.0d0
              elseif ((i .ne. j) .and. (a .eq. b)) then
                 pre = 2.0d0
              else
                 pre = 4.0d0
              endif

              eps = eps_ijb - EpsVirt(a)

              rimp2_energy = rimp2_energy + pre * (g_ij(a,b)**2 + g_ij(b,a)**2 - g_ij(a,b)*g_ij(b,a)) / eps

           enddo
!$acc end loop
        enddo
!$acc end parallel loop

     enddo
  enddo

!$acc end data

  w_time_tot = omp_get_wtime() - w_time_start

  call get_dev_mem ( total_gpu_f,free_gpu_f )

  ! destroy the cublas context
  stat = cublasDestroy_v2 ( cublas_handle )

  print *,''
  write (*,'("GPU-2: GPU memory (init)  = ",F7.2,"  GB   ---  GPU memory (end)  = ",F7.2,"  GB")') &
             & dble(free_gpu_i)/gb,dble(free_gpu_f)/gb
  write (*,'("GPU-2: calculated energy  = ",ES12.4,"  ---  in total time     = ",F7.2,"  s")') rimp2_energy,w_time_tot
  print *,''

  ! deallocate tensors
  deallocate ( g_ij , stat = dealloc_stat )
  if (dealloc_stat .ne. 0) stop "*** problem with deallocation (g_ij) ***"
  deallocate ( EpsVirt , stat = dealloc_stat )
  if (dealloc_stat .ne. 0) stop "*** problem with deallocation (EpsVirt) ***"
  deallocate ( EpsOcc , stat = dealloc_stat )
  if (dealloc_stat .ne. 0) stop "*** problem with deallocation (EpsOcc) ***"
  deallocate ( Calpha , stat = dealloc_stat )
  if (dealloc_stat .ne. 0) stop "*** problem with deallocation (Calpha) ***"

end program rimp2_gpu_2
