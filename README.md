The following eight codes were prepared to accompany 
Chapter 12 of the book 'Parallel Programming with OpenACC', Farber, R., Ed.; Morgan Kaufmann:
Burlington, USA, 2016.

Author: Janus Juul Eriksen

Time: April/May 2016

Place: Aarhus University, Denmark, and San Jose, CA, USA

A Makefile has been prepared for all of the code versions.
These assume that a PGF90 (PGI) compiler is available
(versions 15.10 and 16.4 have been tested).

The tests in gpu_7 and gpu_8 only work with the PGI compiler, as these make use
of CUDA Fortran extensions. If you want to try out the code(s) with other
compilers than those of the PGI suite, please note that CRAY currently does not
support nested OpenACC inside an OpenMP parallel region. This might, however,
be subject to change in the future.

Besides PGF90, an nvcc CUDA compiler must be available, as must a host and a
device math library. In the present tests, MKL and CUBLAS have been used.
If you want to use an alternative host math library (e.g., ATLAS), then
please change the paths in the Makefiles accordingly (i.e., MKL_INCL and MKL_LINK).

/ Janus Juul Eriksen

E-mail 1: janusje [at] chem [dot] au [dot] dk

E-mail 2: januseriksen [at] gmail [dot] com

