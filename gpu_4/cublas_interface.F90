module cublas_interface

  interface

     ! cublasCreate
     integer (c_int) function cublasCreate_v2 ( handle ) bind ( C , name="cublasCreate_v2" )

       use iso_c_binding

       implicit none

       type (c_ptr) :: handle

     end function cublasCreate_v2

     ! cublasDestroy
     integer (c_int) function cublasDestroy_v2 ( handle ) bind ( C , name="cublasDestroy_v2" )

       use iso_c_binding

       implicit none

       type (c_ptr), value :: handle

     end function cublasDestroy_v2

     ! cublasSgemm_v2
     integer (c_int) function cublasSgemm_v2 ( handle,transa,transb,m,n,k,alpha,a,&
                                    & lda,b,ldb,beta,c,ldc ) bind ( C , name="cublasSgemm_v2" )

       use iso_c_binding

       implicit none

       type (c_ptr), value :: handle
       type (c_ptr), value :: a, b, c
       integer (c_int), value :: m, n, k, lda, ldb, ldc
       integer (c_int), value :: transa, transb
       real (c_float) :: alpha, beta

     end function cublasSgemm_v2

     ! cublasDgemm_v2
     integer (c_int) function cublasDgemm_v2 ( handle,transa,transb,m,n,k,alpha,a,&
                                    & lda,b,ldb,beta,c,ldc ) bind ( C , name="cublasDgemm_v2" )

       use iso_c_binding

       implicit none

       type (c_ptr), value :: handle
       type (c_ptr), value :: a, b, c
       integer (c_int), value :: m, n, k, lda, ldb, ldc
       integer (c_int), value :: transa, transb
       real (c_double) :: alpha, beta

     end function cublasDgemm_v2

     ! cublasSetStream_v2
     integer (c_int) function cublasSetStream_v2 ( handle,stream ) bind (C , name="cublasSetStream_v2" )

       use iso_c_binding

       implicit none

       type (C_PTR), value :: handle
       type (C_PTR), value :: stream

     end function cublasSetStream_v2

  end interface

contains

  subroutine sgemm_interface ( transa,transb,m,n,k,alpha,a,lda,b,ldb,beta,c,ldc,na,nb,nc,cublas_handle,gpu )

    use iso_c_binding

    implicit none

    character(len=1), intent(in) :: transa,transb
    integer, intent(in) :: m,n,k,lda,ldb,ldc
    integer, intent(in) :: na,nb,nc
    real(4), dimension(na), intent(in), target :: a
    real(4), dimension(nb), intent(in), target :: b
    real(4), dimension(nc), intent(inout), target :: c
    real(4), intent(in) :: alpha,beta
    type(c_ptr), intent(inout) :: cublas_handle
    logical, intent(in) :: gpu
    integer(4) :: stat
    integer :: transa_2,transb_2

    ! adapt to CUBLAS interface
    transa_2 = 0; transb_2 = 0
    if ((transa .eq. 't') .or. (transa .eq. 'T')) transa_2 = 1
    if ((transb .eq. 't') .or. (transb .eq. 'T')) transb_2 = 1

    if (gpu) then

!$acc host_data use_device(a,b,c)
       stat = cublasSgemm_v2 ( cublas_handle,int(transa_2,kind=4),int(transb_2,kind=4),&
                             & int(m,kind=4),int(n,kind=4),int(k,kind=4),&
                             & alpha,c_loc(a),int(lda,kind=4),c_loc(b),int(ldb,kind=4),&
                             & beta,c_loc(c),int(ldc,kind=4) )
!$acc end host_data

    else

       call sgemm ( transa,transb,m,n,k,alpha,a,lda,b,ldb,beta,c,ldc )

    endif

  end subroutine sgemm_interface

  subroutine dgemm_interface ( transa,transb,m,n,k,alpha,a,lda,b,ldb,beta,c,ldc,na,nb,nc,cublas_handle,gpu )

    use iso_c_binding

    implicit none

    character(len=1), intent(in) :: transa,transb
    integer, intent(in) :: m,n,k,lda,ldb,ldc
    integer, intent(in) :: na,nb,nc
    real(8), dimension(na), intent(in), target :: a
    real(8), dimension(nb), intent(in), target :: b
    real(8), dimension(nc), intent(inout), target :: c
    real(8), intent(in) :: alpha,beta
    type(c_ptr), intent(inout) :: cublas_handle
    logical, intent(in) :: gpu
    integer(4) :: stat
    integer :: transa_2,transb_2

    ! adapt to CUBLAS interface
    transa_2 = 0; transb_2 = 0
    if ((transa .eq. 't') .or. (transa .eq. 'T')) transa_2 = 1
    if ((transb .eq. 't') .or. (transb .eq. 'T')) transb_2 = 1

    if (gpu) then

!$acc host_data use_device(a,b,c)
       stat = cublasDgemm_v2 ( cublas_handle,int(transa_2,kind=4),int(transb_2,kind=4),&
                             & int(m,kind=4),int(n,kind=4),int(k,kind=4),&
                             & alpha,c_loc(a),int(lda,kind=4),c_loc(b),int(ldb,kind=4),&
                             & beta,c_loc(c),int(ldc,kind=4) )
!$acc end host_data

    else

       call dgemm ( transa,transb,m,n,k,alpha,a,lda,b,ldb,beta,c,ldc )

    endif

  end subroutine dgemm_interface

  subroutine cublas_set_stream ( cublas_handle,acc_handle )

    use iso_c_binding

    implicit none

    integer, intent(in) :: acc_handle
    type(c_ptr), intent(inout) :: cublas_handle
    integer(4) :: stat
    type (c_ptr), target :: handle_cptr
    type (c_ptr), external :: acc_get_cuda_stream

    ! do CUBLAS calls on the stream corresponding to acc_handle
    handle_cptr = acc_get_cuda_stream ( acc_handle )
    stat = cublasSetStream_v2 ( cublas_handle,handle_cptr )

  end subroutine cublas_set_stream

end module cublas_interface
